sardonic, S, A, R, D, O, N, I, C, characterized by bitter or scornful derision; mocking; cynical; sneering. For example, a sardonic grin.
remonstrance, R, E, M, O, N, S, T, R, A, N, C, E, to argue in protest or objection. For example, to remonstrate with the government.
vexation, V, E, X, A, T, I, O, N, the state of being vexed; irritation; annoyance. For example, vexation at missing the bus.
formidable, F, O, R, M, I, D, A, B, L, E, causing fear, apprehension, or dread: a formidable opponent.
sinister, S, I, N, I, S, T, E, R, bad, evil, base, or wicked; fell: his sinister purposes.
purview, P, U, R, V, I, E, W, the range of vision, insight, or understanding.
deprecate, D, E, P, R, E, C, A, T, E, to express earnest disapproval of.
pawky, P, A, W, K, Y, having or characterized by a dry wit.
scheme, S, C, H, E, M, E, a plan, design, or program of action to be followed; project.
aspire, A, S, P, I, R, E, to long, aim, or seek ambitiously; be eagerly desirous, especially for something great or of high value.
slader, S, L, A, N, D, E, R, a malicious, false, and defamatory statement or report: a slander against his good name.
convey, C, O, N, V, E, Y, to carry, bring, or take from one place to another; transport; bear.
shrewd, S, H, R, E, W, D, astute or sharp in practical matters. For example, a shrewd politician.
overdue, O, V, E, R, D, U, E, past due, as a delayed train or a bill not paid by the assigned date. For example, two overdue library books.
remark, R, E, M, A, R, K, to say casually, as in making a comment. For example,  Someone remarked that tomorrow would be a warm day.
exultant, E, X, U, L, T, A, N, T, show or feel triumphant elation.
frown, F, R, O, W, N, to contract the brow, as in displeasure or deep thought; scowl.
irksome, I, R, K, S, O, M, E,  annoying; irritating; exasperating; tiresome. Example, irksome restrictions.
penetrate, P, E, N, E, T, R, A, T, E,  to arrive at the truth or meaning of; understand; comprehend. Example, to penetrate a mystery.
machiavellian, M, A, C, H, I, A, V, E, L, L, I, A, N, characterized by subtle or unscrupulous cunning, deception, expediency, or dishonesty. For example, He resorted to Machiavellian tactics in order to get ahead.
hypothesis, H, Y, P, O, T, H, E, S, I, S, a mere assumption or guess.
merely, M, E, R, E, L, Y, only as specified and nothing more; or simply.
subtle, S, U, B, T, L, E,  - delicate or faint and mysterious. Another meaning of the word is, requiring mental acuteness, penetration, or discernment. For example, a subtle philosophy.
scintillate, S, C, I, N, T, I, L, L, A, T, E,  - witty; brilliantly clever. For example, a scintillating conversationalist; a play full of scintillating dialogue.
deceive, D, E, C, E, I, V, E, to mislead by a false appearance or statement; delude: They deceived the enemy by disguising the destroyer as a freighter.
exiguous, E, X, I, G, U, O, U, S, scanty; meager; small; slender. Example, exiguous income.
crude, C, R, U, D, E, in a raw or unprepared state; unrefined or natural. Example, crude sugar.
plausible, P, L, A, U, S, I, B, L, E, having an appearance of truth or reason; seemingly worthy of approval or acceptance; credible; believable. For example, a plausible excuse; a plausible plot.
bespeak, B, E, S, P, E, A, K, to ask for in advance. Example, to bespeak the reader's patience.
stammer, S, T, A, M, M, E, R, to speak with involuntary breaks and pauses, or with spasmodic repetitions of syllables or sounds.
enigmatic, E, N, I, G, M, A, T, I, C, it means perplexing; mysterious.
astonish, A, S, T, O, N, I, S, H, to fill with sudden and overpowering surprise or wonder; amaze. For example, Her easy humor and keen intellect astonished me.
substantial, S, U, B, S, T, A, N, T, I, A, L, of ample or considerable amount, quantity, size. Example, a substantial sum of money.
composure, C, O, M, P, O, S, U, R, E, serene, self-controlled state of mind; calmness; tranquillity. For example, Despite the hysteria and panic around him, he retained his composure.
pretext, P, R, E, T, E, X, T, the misleading appearance or behavior assumed with this intention. For example, His many lavish compliments were a pretext for subtle mockery.