﻿using System.Collections.Generic;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.IO;
using System;

namespace LUCY
{
    public partial class Main : Form
    {
        void CONSTANTS_SpeechRecognized(object sender, SpeechRecognizedEventArgs defaultEvent)
        {
            String speech = defaultEvent.Result.Text;
            int randomNumber = random.Next(1,5);

            switch (speech)
            {
                case "lucy":
                    if (randomNumber == 1)      { LUCY.SpeakAsync("Yes?"); }
                    else if (randomNumber == 2) { LUCY.SpeakAsync("Sir?"); }
                    else if (randomNumber == 3) { LUCY.SpeakAsync("What is it sir?"); }
                    else if (randomNumber == 4) { LUCY.SpeakAsync("What can I do for your sir?"); }
                    else if (randomNumber == 5) { LUCY.SpeakAsync("Yes sir, how may I help"); }
                break;

                case "whats your name":
                    LUCY.SpeakAsync("My name is Lucy.");
                    RepeatSpokenWords = "I said, my name is Lucy.";
                break;

                case "sorry what":
                case "again":
                case "repeat":
                case "pardon":
                    LUCY.SpeakAsync(RepeatSpokenWords);
                break;

                case "identify":
                    LUCY.SpeakAsync("I am Lucy. An Artificial Intelligence, Prototype One. Created by Bryan Chu using C# Programming.");
                break;

                case "what time is it":
                case "whats the time":
                    to = DateTime.Now;
                    string time = to.GetDateTimeFormats('t')[0];
                    LUCY.SpeakAsync(time + " sir");
                    RepeatSpokenWords = time;
                break;

                case "today is":
                case "what day is it":
                    LUCY.SpeakAsync("Today is " + DateTime.Today.ToString("dddd"));
                    RepeatSpokenWords = DateTime.Today.ToString("dddd");
                break;

                case "whats the date":
                    LUCY.SpeakAsync(DateTime.Now.ToString("dddd MMMM dd yyyy"));
                    RepeatSpokenWords = DateTime.Now.ToString("dddd MMMM dd yyyy");
                break;

                case "reminders":
                    Reminders();
                break;

                case "what year is it":
                    LUCY.SpeakAsync("Year " + DateTime.Now.Year + " Sir.");
                    RepeatSpokenWords = DateTime.Now.Year.ToString();
                break;

                case "leap year or not":
                case "leap year":
                    if (DateTime.IsLeapYear(DateTime.Now.Year)) { LUCY.SpeakAsync("Yes, sir. " + DateTime.Now.Year + " is a leap year."); }
                    else { LUCY.SpeakAsync("No sir, " + DateTime.Now.Year + " is not a leap year."); }
                break;
                    
                case "will you stop":
                case "stop reminding me":
                case "stop asking me":
                case "stop asking":
                    LUCY.SpeakAsync("As you wish sir.");
                    RepeatSpokenWords = "I said, as you wish sir.";
                    TimeUpdater.Stop();
                break;

                case "thank you":
                case "thanks":
                    EventStatus = String.Empty;
                    if (randomNumber == 1) { LUCY.SpeakAsync("You're always welcome sir"); }
                    else if (randomNumber == 2) { LUCY.SpeakAsync("Pleasure is mine sir."); }
                    else { LUCY.SpeakAsync("You're welcome."); }
                    RepeatSpokenWords = "I said, you're welcome sir.";
                break;

                case "close":
                    SendKeys.Send("%{F4}");
                break;

                case "comfirm":
                    SendKeys.Send("{ENTER}");
                break;

                case "how many songs":
                case "how many music":
                    try {
                        int count = ListOfMusicNames.Length;
                        if (count > 0)
                        {
                            LUCY.SpeakAsync("There are " + count + " songs in Music folder sir.");
                            RepeatSpokenWords = "I said, there are " + count + " songs sir.";
                        }
                        else
                        {
                            LUCY.SpeakAsync("There are no songs in your music folder sir.");
                            RepeatSpokenWords = "There are no songs in your music folder sir.";
                        }
                    }
                    catch {
                        LUCY.SpeakAsync("There are no songs in your music folder sir.");
                        RepeatSpokenWords = "There are no songs in your music folder sir.";
                    }
                break;

                case "write down all music":
                    POPULATE_MUSIC();
                break;

                case "update knowledge base":
                    UPDATE_GRAMMAR();
                break;

                case "add reminder":
                    Reminder_Window rw = new Reminder_Window();
                    rw.Show();
                break;

                case "add schedule":
                    Schedule_Window sw = new Schedule_Window();
                    sw.Show();
                break;

                case "add new word definition":
                    Dictionary_Window dw = new Dictionary_Window();
                    dw.Show();
                break;

                case "terminate program":
                    Close();
                break;

                case "reboot program":
                    Process.Start(Application.ExecutablePath);
                    this.Close();
                break;

                case "stop":
                case "quiet":
                    if (LUCY.State == SynthesizerState.Ready) { LUCY.SpeakAsync("Sir I am not saying anything."); }
                    else{ LUCY.SpeakAsyncCancelAll(); LUCY.SpeakAsync("Sorry!"); }
                break;

                case "inactive listening":
                    LUCY.SpeakAsync("Listening mode inactive");
                    speechMemory.RecognizeAsyncCancel();
                    activateListening.RecognizeAsync(RecognizeMode.Multiple);
                break;

                case "pause":
                    if (LUCY.State == SynthesizerState.Speaking){ LUCY.Pause(); }
                break;

                case "resume":
                    if (LUCY.State == SynthesizerState.Paused){ LUCY.Resume(); }
                break;

                case "mute":
                    MUTE_SOUND();
                break;

                case "unmute":
                    UNMUTE_SOUND();
                break;

                case "abort":
                    LUCY.SpeakAsync("System termination cancelled.");
                    Process.Start("shutdown", "/a");
                break;
            }
        }

        void activateListening_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            string speech = e.Result.Text;
            switch (speech)
            {
                case "active listening":
                    speechMemory.RecognizeAsync(RecognizeMode.Multiple);
                    activateListening.RecognizeAsyncCancel();
                    LUCY.SpeakAsync("Listening mode active");
                    break;
            }
        }

        void POPULATE_MUSIC()
        {
            
            try
            {
                foreach (string file in ActualMusicFilePaths)
                {
                    string filename = Path.GetFileNameWithoutExtension(file);
                    filename = characters.Replace(filename, @"");
                    filename = space.Replace(filename, @" ").ToLower();
                    ActualMusicFileTitles[index] = filename;
                    index++;
                }

                File.WriteAllLines(MusicNamesPath, ActualMusicFileTitles);
                File.WriteAllLines(MusicFilePaths, ActualMusicFilePaths);

                LUCY.Speak("Music titles completely updated sir.");
            }
            catch
            {
                Process.Start(Application.ExecutablePath);
                this.Close();
            }

        }

        void UPDATE_GRAMMAR()
        {
            LUCY.SpeakAsync("Updating knowledge base, please wait for a moment.");

            ListOfChatQueries                   = File.ReadAllLines(ChatQueriesPath);
            ListOfChatResponse                  = File.ReadAllLines(ChatResponsesPath);
            ListOfWebQueries                    = File.ReadAllLines(WebQueriesPath);
            ListOfWebURL                        = File.ReadAllLines(WebURLPath);
            ListOfShellQueries                  = File.ReadAllLines(shellQueriesPath);
            ListOfShellResponses                = File.ReadAllLines(shellResponsesPath);
            ListOfShellPath                     = File.ReadAllLines(shellFilePath);
            ListOfDefinitionQueries             = File.ReadAllLines(DefinitionQueriesPath);
            ListOfWordDefinition                = File.ReadAllLines(WordDefinitionsPath);
            ListOfProcessKillQuery              = File.ReadAllLines(ProcessKillQueriesPath);
            ListOfProcessName                   = File.ReadAllLines(ProcessNamesPath);
            ListOfReadQueries                   = File.ReadAllLines(ReadQueriesPath);
            ListOfReadingPath                   = File.ReadAllLines(ReadingFilesPath);
            ListOfSendkeysQueries               = File.ReadAllLines(SendkeyQueriesPath);
            ListOfSendkeysCode                  = File.ReadAllLines(SendKeysCodePath);
            ListOfMusicNames                    = File.ReadAllLines(MusicNamesPath);
            ListOfMusicPaths                    = File.ReadAllLines(MusicFilePaths);
            ListOfIdeaQuery                     = File.ReadAllLines(IdeaQueryPath);
            ListOf1stPhrase                     = File.ReadAllLines(FirstPhrasePath);
            ListOf2ndPhrase                     = File.ReadAllLines(SecondPhrasePath);
            ListOfQueryToViewText               = File.ReadAllLines(PathOfQueryToViewText);
            ListOfTextFilePaths                 = File.ReadAllLines(PathOfTextFilePaths);
            ListOfQueryForRandomResponse        = File.ReadAllLines(QueryForRandomResponsePath);
            ListOfFileLocationsRandomResponses  = File.ReadAllLines(RandomResponsesFileLocationsPath);
            ListOfSchedules                     = File.ReadAllLines(ListOfSchedulesPath);
            ListOfReminders                     = File.ReadAllLines(ListOfRemindersPath);

            try
            {
                ChatGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfChatQueries)));
                speechMemory.LoadGrammar(ChatGrammar);
            }
            catch { }

            try
            {
                WebGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfWebQueries)));
                speechMemory.LoadGrammar(WebGrammar);
            }
            catch { }

            try
            {
                ShellGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfShellQueries)));
                speechMemory.LoadGrammar(ShellGrammar);
            }
            catch { }

            try
            {
                DefinitionGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfDefinitionQueries)));
                speechMemory.LoadGrammar(DefinitionGrammar);
            }
            catch { }

            try
            {
                KillProcessGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfProcessKillQuery)));
                speechMemory.LoadGrammar(KillProcessGrammar);
            }
            catch { }

            try
            {
                ReadFileGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfReadQueries)));
                speechMemory.LoadGrammar(ReadFileGrammar);
            }
            catch { }

            try
            {
                SendKeysGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfSendkeysQueries)));
                speechMemory.LoadGrammar(SendKeysGrammar);
            }
            catch { }

            try
            {
                MusicGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfMusicNames)));
                speechMemory.LoadGrammar(MusicGrammar);
            }
            catch { }

            try
            {
                RandomizerGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfIdeaQuery)));
                speechMemory.LoadGrammar(RandomizerGrammar);
            }
            catch { }

            try
            {
                DocumentViewerGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfQueryToViewText)));
                speechMemory.LoadGrammar(DocumentViewerGrammar);
            }
            catch { }

            try
            {
                QueryForRandomResponseGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfQueryForRandomResponse)));
                speechMemory.LoadGrammar(QueryForRandomResponseGrammar);
            }
            catch { }

            LUCY.SpeakAsync("Knowledge base completely updated sir.");
        }

    }
}