﻿using System.Collections.Generic;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Data;
using System.IO;
using System;

namespace LUCY
{
    public partial class Schedule_Window : Form
    {

        string SchedulePath = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Schedule.txt";
        int count;

        public Schedule_Window() { InitializeComponent(); }

        private void Save_Schedule_Click(object sender, EventArgs e)
        {
            string DAY          = DayPicker.Value.ToString("dddd");
            string [] MORNING   = MorningTextBox.Text.Split('\n');
            string [] AFTERNOON = AfternoonTextBox.Text.Split('\n');

            using (StreamWriter writer = File.AppendText(SchedulePath))
            {
                writer.Write(DAY + "|");
                count = 0;

                foreach (string line in MORNING)
                {
                    writer.Write(line);
                    if (count != MORNING.Length - 1) { writer.Write(","); }
                    else { writer.Write(';'); }
                    count++;
                }

                count = 0;

                foreach (string line in AFTERNOON)
                {
                    writer.Write(line);
                    if (count != AFTERNOON.Length - 1) { writer.Write(","); }
                    count++;
                }

                writer.WriteLine();
            }

            MorningTextBox.Text = String.Empty;
            AfternoonTextBox.Text = String.Empty;
            MessageBox.Show("Schedule added.");
        }
    }
}