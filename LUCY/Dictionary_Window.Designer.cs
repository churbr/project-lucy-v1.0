﻿namespace LUCY
{
    partial class Dictionary_Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dictionary_Window));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Word_Box = new System.Windows.Forms.RichTextBox();
            this.Definition_Box = new System.Windows.Forms.RichTextBox();
            this.Example_Box = new System.Windows.Forms.RichTextBox();
            this.Save_Entry = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "WORD:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "DEFINITION:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 164);
            this.label3.MaximumSize = new System.Drawing.Size(100, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 44);
            this.label3.TabIndex = 2;
            this.label3.Text = "EXAMPLE OF USAGE:";
            // 
            // Word_Box
            // 
            this.Word_Box.Location = new System.Drawing.Point(123, 25);
            this.Word_Box.Name = "Word_Box";
            this.Word_Box.Size = new System.Drawing.Size(175, 34);
            this.Word_Box.TabIndex = 3;
            this.Word_Box.Text = "";
            // 
            // Definition_Box
            // 
            this.Definition_Box.Location = new System.Drawing.Point(123, 81);
            this.Definition_Box.Name = "Definition_Box";
            this.Definition_Box.Size = new System.Drawing.Size(470, 70);
            this.Definition_Box.TabIndex = 4;
            this.Definition_Box.Text = "";
            // 
            // Example_Box
            // 
            this.Example_Box.Location = new System.Drawing.Point(123, 164);
            this.Example_Box.Name = "Example_Box";
            this.Example_Box.Size = new System.Drawing.Size(470, 70);
            this.Example_Box.TabIndex = 5;
            this.Example_Box.Text = "";
            // 
            // Save_Entry
            // 
            this.Save_Entry.BackColor = System.Drawing.Color.Gainsboro;
            this.Save_Entry.Location = new System.Drawing.Point(270, 245);
            this.Save_Entry.Name = "Save_Entry";
            this.Save_Entry.Size = new System.Drawing.Size(141, 38);
            this.Save_Entry.TabIndex = 6;
            this.Save_Entry.Text = "SAVE ENTRY";
            this.Save_Entry.UseVisualStyleBackColor = false;
            this.Save_Entry.Click += new System.EventHandler(this.Save_Entry_Click);
            // 
            // Dictionary_Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(605, 295);
            this.Controls.Add(this.Save_Entry);
            this.Controls.Add(this.Example_Box);
            this.Controls.Add(this.Definition_Box);
            this.Controls.Add(this.Word_Box);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Dictionary_Window";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ADD NEW DICTIONARY ENTRY";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox Word_Box;
        private System.Windows.Forms.RichTextBox Definition_Box;
        private System.Windows.Forms.RichTextBox Example_Box;
        private System.Windows.Forms.Button Save_Entry;
    }
}