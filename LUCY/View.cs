﻿using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System;

namespace LUCY
{
    public partial class Main : Form
    {

        void OPENFILE_SpeechRecognized(object sender, SpeechRecognizedEventArgs openFileEvent)
        {

            string speech = openFileEvent.Result.Text;
            index = 0;

            if (speech == "hide window")
            {
                TextDisplay.Clear();
                this.Opacity = 0;
                this.WindowState = FormWindowState.Minimized;
                this.FormBorderStyle = FormBorderStyle.SizableToolWindow;
                this.TextDisplay.Visible = false; 
            }
            else if (speech == "read file")
            {
                if (TextDisplay.Text == String.Empty)
                {
                    RepeatSpokenWords = "I said, nothing is to be read sir.";
                    LUCY.SpeakAsync("Nothing to read sir.");
                }
                else
                {
                    RepeatSpokenWords = TextDisplay.Text;
                    LUCY.SpeakAsync(TextDisplay.Text);
                }
            }
            else if (speech == "save file")
            {
                if (TextDisplay.Text == String.Empty)
                {
                    RepeatSpokenWords = "I said, nothing is to be saved sir.";
                    LUCY.SpeakAsync("Nothing to save sir.");
                }
                else
                {
                    RepeatSpokenWords = "I said, file is saved.";
                    File.WriteAllText(File_Location, TextDisplay.Text);
                    LUCY.SpeakAsync("File saved.");
                }
            }
            else{

                try{
                    foreach (string line in ListOfQueryToViewText)
                    {
                        if (line == speech)
                        {
                            numberDisplay = String.Empty;
                            numberStr = String.Empty;
                            EventStatus = String.Empty;
                            MULTIPLICATION_RESULT = 1;
                            ADDITION_RESULT = 0;
                            MINUEND = 0;
                            SUBTRAHEND = 0;
                            DIFFERENCE = 0;
                            DIVIDEND = 0;
                            DIVISOR = 0;
                            QUOTIENT = 0;
                            INPUT = 0;
                            TOTAL = 0;
                            PERCENTAGE = 0;
                            NumericForm.Clear();
                            Display.Text = String.Empty;
                            this.Opacity = 0;
                            this.WindowState = FormWindowState.Minimized;
                            this.WindowState = FormWindowState.Normal;
                            this.FormBorderStyle = FormBorderStyle.Fixed3D;
                            this.TextDisplay.Visible = true;
                            this.Opacity = 100;

                            TextDisplay.Clear();
                            TextDisplay.Visible = true;
                            File_Location = ListOfTextFilePaths[index];
                            TextDisplay.LoadFile(ListOfTextFilePaths[index], RichTextBoxStreamType.PlainText);
                            RepeatSpokenWords = "File contained sir.";
                            LUCY.SpeakAsync("File contained.");
                        }
                        index++;
                    }
                }
                catch { }
            }
        }
    }
}