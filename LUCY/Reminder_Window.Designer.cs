﻿namespace LUCY
{
    partial class Reminder_Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Reminder_Window));
            this.Save_Reminder = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ReminderInput = new System.Windows.Forms.RichTextBox();
            this.datePicked = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // Save_Reminder
            // 
            this.Save_Reminder.Location = new System.Drawing.Point(76, 256);
            this.Save_Reminder.Name = "Save_Reminder";
            this.Save_Reminder.Size = new System.Drawing.Size(159, 49);
            this.Save_Reminder.TabIndex = 0;
            this.Save_Reminder.Text = "SAVE REMINDER";
            this.Save_Reminder.UseVisualStyleBackColor = true;
            this.Save_Reminder.Click += new System.EventHandler(this.Save_Reminder_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Date:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Reminder:";
            // 
            // ReminderInput
            // 
            this.ReminderInput.Location = new System.Drawing.Point(40, 133);
            this.ReminderInput.Name = "ReminderInput";
            this.ReminderInput.Size = new System.Drawing.Size(245, 117);
            this.ReminderInput.TabIndex = 3;
            this.ReminderInput.Text = "";
            // 
            // datePicked
            // 
            this.datePicked.CustomFormat = "MMMM dd yyyy";
            this.datePicked.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datePicked.Location = new System.Drawing.Point(40, 58);
            this.datePicked.Name = "datePicked";
            this.datePicked.Size = new System.Drawing.Size(245, 25);
            this.datePicked.TabIndex = 4;
            // 
            // Reminder_Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 308);
            this.Controls.Add(this.datePicked);
            this.Controls.Add(this.ReminderInput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Save_Reminder);
            this.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Reminder_Window";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ADD REMINDER";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Save_Reminder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox ReminderInput;
        private System.Windows.Forms.DateTimePicker datePicked;
    }
}