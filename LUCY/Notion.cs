﻿using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System;

namespace LUCY
{
    public partial class Main : Form
    {

        void RANDOMIZER_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            string speech = e.Result.Text;
            index = 0;

            if (speech == "any idea" || speech == "what do you think")
            {
                EventStatus = "Query";
                RepeatSpokenWords = "I said, about what sir.";
                LUCY.Speak("About what sir?");
            }

            else if (speech == "what else")
            {
                if (EventStatus == "Query")
                {
                    EventStatus = "Else";
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////

            if (EventStatus == "Query")
            {
                foreach (string line in ListOfIdeaQuery)
                {
                    try
                    {
                        if (line == speech)
                        {
                            FirstPhrase = File.ReadAllLines(ListOf1stPhrase[index]);
                            SecondPhrase = File.ReadAllLines(ListOf2ndPhrase[index]);

                            FirstPhraseLineLimit = FirstPhrase.Length;
                            SecondPhraseLineLimit = SecondPhrase.Length;

                            int randomed1 = random.Next(0, FirstPhraseLineLimit);
                            int randomed2 = random.Next(0, SecondPhraseLineLimit);

                            RepeatSpokenWords = "I said, " + FirstPhrase[randomed1] + " " + SecondPhrase[randomed2];
                            LUCY.Speak(FirstPhrase[randomed1] + " " + SecondPhrase[randomed2]);

                            currentIndex = index;
                        }
                        index++;
                    }
                    catch { }
                }
            }

            else if (EventStatus == "Else")
            {
                try
                {
                    FirstPhrase = File.ReadAllLines(ListOf1stPhrase[currentIndex]);
                    SecondPhrase = File.ReadAllLines(ListOf2ndPhrase[currentIndex]);

                    int randomed1 = random.Next(0, FirstPhraseLineLimit);
                    int randomed2 = random.Next(0, SecondPhraseLineLimit);

                    RepeatSpokenWords = "I said, " + FirstPhrase[randomed1] + " " + SecondPhrase[randomed2];
                    LUCY.Speak(FirstPhrase[randomed1] + " " + SecondPhrase[randomed2]);

                }
                catch { }
            }
        }

    }
}