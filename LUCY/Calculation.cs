﻿using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System;

namespace LUCY
{
    public partial class Main : Form
    {

        void CALCULATE_SpeechRecognized(object sender, SpeechRecognizedEventArgs calculateEvent)
        {
            string speech = calculateEvent.Result.Text;

            if (speech == "run calculation")
            {
                TextDisplay.Clear();
                this.Opacity = 0;
                this.WindowState = FormWindowState.Minimized;
                this.TextDisplay.Visible = false;
                this.Opacity = 100;
                this.WindowState = FormWindowState.Normal;
                this.FormBorderStyle = FormBorderStyle.Fixed3D;
                LUCY.SpeakAsync("What type of operation sir?");
                RepeatSpokenWords = "What type of operation sir?";
                EventStatus = "calculation event";
            }

            if (EventStatus == "calculation event")
            {
                if (speech == "addition")
                {
                    EventStatus = "addition event";
                    RepeatSpokenWords = "Event status addition. You may now say a number sir.";
                    LUCY.SpeakAsync("Event status addition. You may now say a number sir.");
                }

                else if (speech == "multiplication")
                {
                    EventStatus = "multiplication event";
                    RepeatSpokenWords = "Event status multiplication. You may now say a number sir.";
                    LUCY.SpeakAsync("Event status multiplication. You may now say a number sir.");
                }

                else if (speech == "subtraction")
                {
                    EventStatus = "subtraction event";
                    RepeatSpokenWords = "Event status subtraction. You may now say a number sir.";
                    LUCY.SpeakAsync("Event status subtraction. You may now say a number sir.");
                }

                else if (speech == "division")
                {
                    EventStatus = "division event";
                    RepeatSpokenWords = "Event status division. You may now say a number sir.";
                    LUCY.SpeakAsync("Event status division. You may now say a number sir.");
                }

                else if (speech == "percentage")
                {
                    EventStatus = "percentage event";
                    RepeatSpokenWords = "Event status percentage calculation. You may now say a number sir.";
                    LUCY.SpeakAsync("Event status percentage calculation. You may now say a number sir.");
                }
            }

            if (speech == "clear")
            {
                numberDisplay = String.Empty;
                numberStr = String.Empty;
                EventStatus = String.Empty;
                MULTIPLICATION_RESULT = 1;
                ADDITION_RESULT = 0;
                MINUEND = 0;
                SUBTRAHEND = 0;
                DIFFERENCE = 0;
                DIVIDEND = 0;
                DIVISOR = 0;
                QUOTIENT = 0;
                INPUT = 0;
                TOTAL = 0;
                PERCENTAGE = 0;
                NumericForm.Clear();
                Display.Text = String.Empty;
                this.Opacity = 0;
                this.WindowState = FormWindowState.Minimized;
                this.FormBorderStyle = FormBorderStyle.SizableToolWindow;
                RepeatSpokenWords = "I said, all variables are now cleared sir.";
                LUCY.SpeakAsync("All variables are cleared sir.");
            }

            // ################################# A D D I T I O N  E V E N T #################################

            try
            {
                if (EventStatus == "addition event")
                {

                    switch (speech)
                    {

                        case "zero":
                            numberDisplay+="0";
                            Display.Text = numberDisplay;
                            numberStr += "0";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "one":
                            numberDisplay+="1";
                            Display.Text = numberDisplay;
                            numberStr += "1";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "two":
                            numberDisplay+="2";
                            Display.Text = numberDisplay;
                            numberStr += "2";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "three":
                            numberDisplay+="3";
                            Display.Text = numberDisplay;
                            numberStr += "3";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "four":
                            numberDisplay+="4";
                            Display.Text = numberDisplay;
                            numberStr += "4";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "five":
                            numberDisplay+="5";
                            Display.Text = numberDisplay;
                            numberStr += "5";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "six":
                            numberDisplay+="6";
                            Display.Text = numberDisplay;
                            numberStr += "6";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "seven":
                            numberDisplay+="7";
                            Display.Text = numberDisplay;
                            numberStr += "7";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "eight":
                            numberDisplay+="8";
                            Display.Text = numberDisplay;
                            numberStr += "8";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "nine":
                            numberDisplay+="9";
                            Display.Text = numberDisplay;
                            numberStr += "9";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "backspace":
                            numberStr = numberStr.Remove(numberStr.Length - 1);
                            numberDisplay = numberDisplay.Remove(numberDisplay.Length - 1);
                            Display.Text = numberDisplay;
                            break;

                        case "plus":

                            if (numberStr == String.Empty)
                            {
                                RepeatSpokenWords = "I said, no number is mentioned yet sir";
                                LUCY.SpeakAsync("No number mentioned yet sir.");
                            }
                            else
                            {
                                numberDisplay += " + ";
                                Display.Text = numberDisplay;
                                double convertedNumber = Convert.ToDouble(numberStr);
                                NumericForm.Add(convertedNumber);
                                LUCY.SpeakAsync(convertedNumber + "plus?");
                                numberStr = String.Empty;
                            }

                            break;

                        case "calculate":
                            double convertedLastNum = Convert.ToDouble(numberStr);
                            NumericForm.Add(convertedLastNum);

                            if (NumericForm.Count == 0 || NumericForm.Count == 1)
                            {
                                RepeatSpokenWords = "I said, I need to have atleast 2 numbers to complete the calculation.";
                                LUCY.SpeakAsync("Sir, I need to have atleast 2 numbers to complete the calculation.");
                                NumericForm.Clear();
                                ADDITION_RESULT = 0;
                                numberStr = String.Empty;
                                numberDisplay = String.Empty;
                                Display.Text = String.Empty;
                            }
                            else
                            {
                                foreach (double eachLine in NumericForm) { ADDITION_RESULT += eachLine; }
                                numberDisplay += " = " + ADDITION_RESULT;
                                Display.Text = numberDisplay;
                                RepeatSpokenWords = "I said sum is equal to " + ADDITION_RESULT;
                                LUCY.SpeakAsync("Sum is equal to " + ADDITION_RESULT);
                                NumericForm.Clear();
                                ADDITION_RESULT = 0;
                                numberStr = String.Empty;
                            }
                            break;
                    }
                }
            }
            catch { }

            /////////////////////////////////////////////////////////////////////////////////////////


            // ######################## M U L T I P L I C A T I O N  E V E N T ########################

            try
            {
                if (EventStatus == "multiplication event")
                {

                    switch (speech)
                    {

                        case "zero":
                            numberDisplay+="0";
                            Display.Text = numberDisplay;
                            numberStr += "0";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "one":
                            numberDisplay+="1";
                            Display.Text = numberDisplay;
                            numberStr += "1";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "two":
                            numberDisplay+="2";
                            Display.Text = numberDisplay;
                            numberStr += "2";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "three":
                            numberDisplay+="3";
                            Display.Text = numberDisplay;
                            numberStr += "3";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "four":
                            numberDisplay+="4";
                            Display.Text = numberDisplay;
                            numberStr += "4";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "five":
                            numberDisplay+="5";
                            Display.Text = numberDisplay;
                            numberStr += "5";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "six":
                            numberDisplay+="6";
                            Display.Text = numberDisplay;
                            numberStr += "6";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "seven":
                            numberDisplay+="7";
                            Display.Text = numberDisplay;
                            numberStr += "7";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "eight":
                            numberDisplay+="8";
                            Display.Text = numberDisplay;
                            numberStr += "8";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "nine":
                            numberDisplay+="9";
                            Display.Text = numberDisplay;
                            numberStr += "9";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "backspace":
                            numberStr = numberStr.Remove(numberStr.Length - 1);
                            numberDisplay = numberDisplay.Remove(numberDisplay.Length - 1);
                            Display.Text = numberDisplay;
                            break;

                        case "times":
                        case "multiply":

                            if (numberStr == String.Empty)
                            {
                                RepeatSpokenWords = "I said, no number is mentioned yet sir.";
                                LUCY.SpeakAsync("No number mentioned yet sir.");
                            }
                            else
                            {
                                numberDisplay += " * ";
                                Display.Text = numberDisplay;
                                double convertedNumber = Convert.ToDouble(numberStr);
                                NumericForm.Add(convertedNumber);
                                LUCY.SpeakAsync(convertedNumber + "multiplied by?");
                                numberStr = String.Empty;
                            }

                            break;

                        case "calculate":
                            double convertedLastNum = Convert.ToDouble(numberStr);
                            NumericForm.Add(convertedLastNum);

                            if (NumericForm.Count == 0 || NumericForm.Count == 1)
                            {
                                RepeatSpokenWords = "I said I need to have atleast 2 numbers to complete the calculation";
                                LUCY.SpeakAsync("Sir, I need to have atleast 2 numbers to complete the calculation.");
                                NumericForm.Clear();
                                MULTIPLICATION_RESULT = 1;
                                numberStr = String.Empty;
                                numberDisplay = String.Empty;
                                Display.Text = String.Empty;
                            }
                            else
                            {
                                foreach (double eachLine in NumericForm) { MULTIPLICATION_RESULT *= eachLine; }
                                numberDisplay += " = " + MULTIPLICATION_RESULT;
                                Display.Text = numberDisplay;
                                RepeatSpokenWords = "I said, product is equal to " + MULTIPLICATION_RESULT;
                                LUCY.SpeakAsync("Product is equal to " + MULTIPLICATION_RESULT);
                                NumericForm.Clear();
                                MULTIPLICATION_RESULT = 1;
                                numberStr = String.Empty;
                            }
                            break;
                    }
                }
            }
            catch { }

            /////////////////////////////////////////////////////////////////////////////////////////


            // ######################## S U B T R A C T I O N  E V E N T ########################

            try
            {
                if (EventStatus == "subtraction event")
                {

                    switch (speech)
                    {

                        case "zero":
                            numberDisplay+="0";
                            Display.Text = numberDisplay;
                            numberStr += "0";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "one":
                            numberDisplay+="1";
                            Display.Text = numberDisplay;
                            numberStr += "1";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "two":
                            numberDisplay+="2";
                            Display.Text = numberDisplay;
                            numberStr += "2";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "three":
                            numberDisplay+="3";
                            Display.Text = numberDisplay;
                            numberStr += "3";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "four":
                            numberDisplay+="4";
                            Display.Text = numberDisplay;
                            numberStr += "4";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "five":
                            numberDisplay+="5";
                            Display.Text = numberDisplay;
                            numberStr += "5";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "six":
                            numberDisplay+="6";
                            Display.Text = numberDisplay;
                            numberStr += "6";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "seven":
                            numberDisplay+="7";
                            Display.Text = numberDisplay;
                            numberStr += "7";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "eight":
                            numberDisplay+="8";
                            Display.Text = numberDisplay;
                            numberStr += "8";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "nine":
                            numberDisplay+="9";
                            Display.Text = numberDisplay;
                            numberStr += "9";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "backspace":
                            numberStr = numberStr.Remove(numberStr.Length - 1);
                            numberDisplay = numberDisplay.Remove(numberDisplay.Length - 1);
                            Display.Text = numberDisplay;
                            break;

                        case "minus":

                            if (numberStr == String.Empty)
                            {
                                RepeatSpokenWords = "I said, no number is mentioned yet sir.";
                                LUCY.SpeakAsync("No number mentioned yet sir.");
                            }
                            else
                            {
                                if (MINUEND == 0)
                                {
                                    numberDisplay += " - ";
                                    Display.Text = numberDisplay;
                                    double convertedNumber = Convert.ToDouble(numberStr);
                                    MINUEND = convertedNumber;
                                    LUCY.SpeakAsync(convertedNumber + "minus?");
                                    numberStr = String.Empty;
                                }
                                else
                                {
                                    numberDisplay += " - ";
                                    Display.Text = numberDisplay;
                                    double convertedNumber = Convert.ToDouble(numberStr);
                                    NumericForm.Add(convertedNumber);
                                    LUCY.SpeakAsync("minus?");
                                    numberStr = String.Empty;
                                }
                            }

                            break;

                        case "calculate":
                            double convertedLastNum = Convert.ToDouble(numberStr);
                            NumericForm.Add(convertedLastNum);

                            if (!NumericForm.Any())
                            {
                                RepeatSpokenWords = "I said, I need to have atleast 2 numbers to complete the calculation.";
                                LUCY.SpeakAsync("Sir, I need to have atleast 2 numbers to complete the calculation.");
                                NumericForm.Clear();
                                MINUEND = 0;
                                SUBTRAHEND = 0;
                                DIFFERENCE = 0;
                                numberStr = String.Empty;
                                numberDisplay = String.Empty;
                                Display.Text = String.Empty;
                            }
                            else
                            {
                                foreach (double eachLine in NumericForm) { SUBTRAHEND += eachLine; }
                                DIFFERENCE = MINUEND - SUBTRAHEND;
                                numberDisplay += " = " + DIFFERENCE;
                                Display.Text = numberDisplay;
                                RepeatSpokenWords = "I said, difference is equal to " + DIFFERENCE;
                                LUCY.SpeakAsync("Difference is equal to " + DIFFERENCE);
                                numberStr = String.Empty;
                                NumericForm.Clear();
                                MINUEND = 0;
                                SUBTRAHEND = 0;
                                DIFFERENCE = 0;
                            }
                            break;
                    }
                }
            }
            catch { }

            /////////////////////////////////////////////////////////////////////////////////////////

            // ############################ D I V I S I O N  E V E N T ############################

            try
            {
                if (EventStatus == "division event")
                {

                    switch (speech)
                    {

                        case "zero":
                            numberDisplay+="0";
                            Display.Text = numberDisplay;
                            numberStr += "0";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "one":
                            numberDisplay+="1";
                            Display.Text = numberDisplay;
                            numberStr += "1";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "two":
                            numberDisplay+="2";
                            Display.Text = numberDisplay;
                            numberStr += "2";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "three":
                            numberDisplay+="3";
                            Display.Text = numberDisplay;
                            numberStr += "3";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "four":
                            numberDisplay+="4";
                            Display.Text = numberDisplay;
                            numberStr += "4";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "five":
                            numberDisplay+="5";
                            Display.Text = numberDisplay;
                            numberStr += "5";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "six":
                            numberDisplay+="6";
                            Display.Text = numberDisplay;
                            numberStr += "6";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "seven":
                            numberDisplay+="7";
                            Display.Text = numberDisplay;
                            numberStr += "7";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "eight":
                            numberDisplay+="8";
                            Display.Text = numberDisplay;
                            numberStr += "8";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "nine":
                            numberDisplay+="9";
                            Display.Text = numberDisplay;
                            numberStr += "9";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "backspace":
                            numberStr = numberStr.Remove(numberStr.Length - 1);
                            numberDisplay = numberDisplay.Remove(numberDisplay.Length - 1);
                            Display.Text = numberDisplay;
                            break;

                        case "divide":
                        case "calculate":

                            if (numberStr == String.Empty)
                            {
                                RepeatSpokenWords = "I said, no number is mentioned yet sir.";
                                LUCY.SpeakAsync("No number mentioned yet sir.");
                            }
                            else
                            {
                                if (DIVIDEND == 0)
                                {
                                    numberDisplay += " / ";
                                    Display.Text = numberDisplay;
                                    double convertedNumber = Convert.ToDouble(numberStr);
                                    DIVIDEND = convertedNumber;
                                    LUCY.SpeakAsync(convertedNumber + "divided by?");
                                    numberStr = String.Empty;
                                }
                                else
                                {
                                    double convertedNumber = Convert.ToDouble(numberStr);
                                    DIVISOR = convertedNumber;
                                    QUOTIENT = DIVIDEND / DIVISOR;
                                    numberDisplay += " = " + QUOTIENT;
                                    Display.Text = numberDisplay;
                                    RepeatSpokenWords = DIVIDEND + " divided by " + DIVISOR + " is equal to " + QUOTIENT.ToString("#.##");
                                    LUCY.SpeakAsync(DIVIDEND + " divided by " + DIVISOR + " is equal to " + QUOTIENT.ToString("#.##"));
                                    numberStr = String.Empty;
                                    DIVIDEND = 0;
                                    DIVISOR = 0;
                                    QUOTIENT = 0;
                                }
                            }

                            break;
                    }
                }
            }
            catch { }

            /////////////////////////////////////////////////////////////////////////////////////////


            // ############################ P E R C E N T A G E  E V E N T ############################

            try
            {
                if (EventStatus == "percentage event")
                {

                    switch (speech)
                    {

                        case "zero":
                            numberDisplay+="0";
                            Display.Text = numberDisplay;
                            numberStr += "0";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "one":
                            numberDisplay+="1";
                            Display.Text = numberDisplay;
                            numberStr += "1";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "two":
                            numberDisplay+="2";
                            Display.Text = numberDisplay;
                            numberStr += "2";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "three":
                            numberDisplay+="3";
                            Display.Text = numberDisplay;
                            numberStr += "3";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "four":
                            numberDisplay+="4";
                            Display.Text = numberDisplay;
                            numberStr += "4";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "five":
                            numberDisplay+="5";
                            Display.Text = numberDisplay;
                            numberStr += "5";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "six":
                            numberDisplay+="6";
                            Display.Text = numberDisplay;
                            numberStr += "6";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "seven":
                            numberDisplay+="7";
                            Display.Text = numberDisplay;
                            numberStr += "7";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "eight":
                            numberDisplay+="8";
                            Display.Text = numberDisplay;
                            numberStr += "8";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "nine":
                            numberDisplay+="9";
                            Display.Text = numberDisplay;
                            numberStr += "9";
                            LUCY.SpeakAsync(speech);
                            break;

                        case "backspace":
                            numberStr = numberStr.Remove(numberStr.Length - 1);
                            numberDisplay = numberDisplay.Remove(numberDisplay.Length - 1);
                            Display.Text = numberDisplay;
                            break;

                        case "input":
                        case "calculate":

                            if (numberStr == String.Empty)
                            {
                                RepeatSpokenWords = "I said, no number is mentioned yet sir.";
                                LUCY.SpeakAsync("No number mentioned yet sir.");
                            }
                            else
                            {
                                if (INPUT == 0)
                                {
                                    double convertedNumber = Convert.ToDouble(numberStr);
                                    INPUT = convertedNumber;
                                    numberDisplay += " / ";
                                    Display.Text = numberDisplay;
                                    RepeatSpokenWords = "How much is the total sir? So that I can complete my calculation.";
                                    LUCY.SpeakAsync("Input equals " + convertedNumber + ". How much is the total sir?");
                                    numberStr = String.Empty;
                                }
                                else
                                {
                                    double convertedNumber = Convert.ToDouble(numberStr);
                                    TOTAL = convertedNumber;
                                    PERCENTAGE = (INPUT / TOTAL) * 100;
                                    numberDisplay += " = " + PERCENTAGE;
                                    Display.Text = numberDisplay;
                                    RepeatSpokenWords = "I said the percentage result is equal to " + PERCENTAGE.ToString("#.##") + "percent.";
                                    LUCY.SpeakAsync("Percentage of " + INPUT + " over " + TOTAL + " equals " + PERCENTAGE.ToString("#.##") + "percent.");
                                    numberStr = String.Empty;
                                    INPUT = 0;
                                    TOTAL = 0;
                                    PERCENTAGE = 0;
                                }
                            }

                            break;
                    }
                }
            }
            catch { }

            /////////////////////////////////////////////////////////////////////////////////////////
        }
    }
}