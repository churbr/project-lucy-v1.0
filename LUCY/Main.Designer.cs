﻿namespace LUCY
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.TimeUpdater = new System.Windows.Forms.Timer(this.components);
            this.Display = new System.Windows.Forms.Label();
            this.TextDisplay = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // TimeUpdater
            // 
            this.TimeUpdater.Enabled = true;
            this.TimeUpdater.Interval = 1800000;
            this.TimeUpdater.Tick += new System.EventHandler(this.TimeUpdater_Tick);
            // 
            // Display
            // 
            this.Display.AutoSize = true;
            this.Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Display.ForeColor = System.Drawing.Color.Lime;
            this.Display.Location = new System.Drawing.Point(12, 23);
            this.Display.MaximumSize = new System.Drawing.Size(470, 480);
            this.Display.Name = "Display";
            this.Display.Size = new System.Drawing.Size(0, 37);
            this.Display.TabIndex = 0;
            this.Display.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TextDisplay
            // 
            this.TextDisplay.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextDisplay.Location = new System.Drawing.Point(-1, 0);
            this.TextDisplay.Name = "TextDisplay";
            this.TextDisplay.Size = new System.Drawing.Size(485, 462);
            this.TextDisplay.TabIndex = 1;
            this.TextDisplay.Text = "";
            this.TextDisplay.Visible = false;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(484, 462);
            this.Controls.Add(this.TextDisplay);
            this.Controls.Add(this.Display);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.Opacity = 0D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LUCY";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer TimeUpdater;
        private System.Windows.Forms.Label Display;
        private System.Windows.Forms.RichTextBox TextDisplay;
    }
}

