﻿using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System;

namespace LUCY
{
    public partial class Dictionary_Window : Form
    {
        string WordSaveLocation         = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Words.txt";
        string DefinitionSaveLocation   = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Definition.txt";

        public Dictionary_Window() { InitializeComponent(); }

        private void Save_Entry_Click(object sender, EventArgs e)
        {
            string Word_Input       = Word_Box.Text.ToString().Trim('\r', '\n');
            string Definition_Input = Definition_Box.Text.ToString().Trim('\r', '\n');
            string Example_Input    = Example_Box.Text.ToString().Trim('\r', '\n');

            // ######################## Trim spaces and newlines ######################## //

            Regex newlines = new Regex(@"[\n\r ]{2,}");
            
            Word_Input          = newlines.Replace(Word_Input, @" ");
            Definition_Input    = newlines.Replace(Definition_Input, @" ");
            Example_Input       = newlines.Replace(Example_Input, @" ");

            // ######################################################################### //

            if (Word_Box.Text == String.Empty)
            { MessageBox.Show("Word field must be supplied."); }
            
            else if(Definition_Box.Text == String.Empty)
            { MessageBox.Show("Definition field must be supplied."); }

            else {
                using (StreamWriter writer = File.AppendText(WordSaveLocation))
                { writer.WriteLine("define " + Word_Input); }
                
                using (StreamWriter writer = File.AppendText(DefinitionSaveLocation))
                {
                    int index = 0;
                    string BreakToChars     = Word_Input;
                    string formedString     = String.Empty;
                    string ProcessedString  = String.Empty;
                    BreakToChars.ToCharArray();

                    foreach (char eachChar in BreakToChars)
                    { formedString += BreakToChars[index] + ", "; index++; }

                    ProcessedString = formedString.ToUpper();

                    writer.Write(Word_Input + ", " + ProcessedString + Definition_Input + ". ");

                    if (Example_Input == String.Empty)
                    {
                        writer.WriteLine();
                    }
                    else
                    {
                        writer.Write("For example, " + Example_Input + ".");
                        writer.WriteLine();
                    }
                }

                Word_Box.Text       = String.Empty;
                Definition_Box.Text = String.Empty;
                Example_Box.Text    = String.Empty;
                MessageBox.Show("Entry added.");
            }
        }
    }
}