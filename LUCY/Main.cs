﻿using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System;

namespace LUCY
{
    public partial class Main : Form
    {

        [DllImport("user32.dll")]
        public static extern IntPtr SendMessageW(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

        Grammar ConstantsGrammar, ChatGrammar, WebGrammar, ShellGrammar, DefinitionGrammar, KillProcessGrammar, ReadFileGrammar, SendKeysGrammar, MusicGrammar, RandomizerGrammar, DocumentViewerGrammar, QueryForRandomResponseGrammar;
        
        SpeechRecognitionEngine activateListening = new SpeechRecognitionEngine(new CultureInfo("en-US"));
        SpeechRecognitionEngine speechMemory = new SpeechRecognitionEngine(new CultureInfo("en-US"));
        SpeechSynthesizer LUCY = new SpeechSynthesizer();
        DateTime to = DateTime.Now;
        private const int APPCOMMAND_VOLUME_MUTE = 0x80000;
        private const int APPCOMMAND_VOLUME_UP = 0xA0000;
        private const int WM_APPCOMMAND = 0x319;
        string[] FirstPhrase;
        string[] SecondPhrase;
        string[] randomResponses;
        int FirstPhraseLineLimit;
        int SecondPhraseLineLimit;
        string RepeatSpokenWords;
        string File_Location;
		string ProcessWindow;
        string TerminationEvent;
		string EventStatus;
        string musicEvent;
        string toBeRead;
        
        List<double> NumericForm = new List<double>();
        string numberDisplay = String.Empty;
        string numberStr = String.Empty;
        double ADDITION_RESULT;
        double MULTIPLICATION_RESULT = 1;
        double MINUEND = 0;
        double SUBTRAHEND = 0;
        double DIFFERENCE;
        double DIVIDEND = 0;
        double DIVISOR = 0;
        double QUOTIENT;
        double INPUT = 0;
        double TOTAL = 0;
        double PERCENTAGE = 0;
        int currentIndex;
        int index = 0;

        Regex characters = new Regex(@"[-,()]");
        Regex space = new Regex("[ ]{2,}");
        Random random = new Random();

        string[] ListOfConstantQueries;
        string[] ListOfChatQueries , ListOfChatResponse; 
		string[] ListOfWebQueries , ListOfWebURL;
        string[] ListOfShellQueries, ListOfShellResponses , ListOfShellPath;
        string[] ListOfDefinitionQueries , ListOfWordDefinition;
        string[] ListOfProcessKillQuery , ListOfProcessName;
        string[] ListOfReadQueries , ListOfReadingPath;
        string[] ListOfSendkeysQueries , ListOfSendkeysCode;
        string[] ActualMusicFileTitles, ActualMusicFilePaths;
        string[] ListOfMusicNames, ListOfMusicPaths;
        string[] ListOfIdeaQuery, ListOf1stPhrase, ListOf2ndPhrase;
        string[] ListOfQueryToViewText, ListOfTextFilePaths;
        string[] ListOfQueryForRandomResponse, ListOfFileLocationsRandomResponses;
        string[] ListOfSchedules, ListOfReminders;

        string ConstantQueriesPath                  = @"Constants.txt";
        string directoryName                        = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE";
        string phrase1Dir                           = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\PHRASE 1";
        string phrase2Dir                           = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\PHRASE 2";
        string randomResponseDir                    = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\RANDOM RESPONSE";
        string ChatQueriesPath                      = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Chat.txt";
        string ChatResponsesPath                    = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Chat Response.txt";
        string WebQueriesPath                       = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Web.txt";
        string WebURLPath                           = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Web URL.txt";
        string shellQueriesPath                     = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Shell.txt";
        string shellResponsesPath                   = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Shell Response.txt";
        string shellFilePath                        = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Shell Path.txt";
        string DefinitionQueriesPath                = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Words.txt";
        string WordDefinitionsPath                  = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Definition.txt";
        string ProcessKillQueriesPath               = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Process Kill Command.txt";
        string ProcessNamesPath                     = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Process Name.txt";
        string ReadQueriesPath                      = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Read Command.txt";
        string ReadingFilesPath                     = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Reading.txt";
        string SendkeyQueriesPath                   = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Sendkeys Command.txt";
        string SendKeysCodePath                     = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Sendkeys Code.txt";
        string ActualMusicDirectory                 = @"C:\Users\" + Environment.UserName + "\\Music";
        string MusicNamesPath                       = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Music Title.txt";
        string MusicFilePaths                       = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Music File.txt";
        string IdeaQueryPath                        = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Query.txt";
        string FirstPhrasePath                      = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\PHRASE 1.txt";
        string SecondPhrasePath                     = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\PHRASE 2.txt";
        string PathOfQueryToViewText                = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Query To View Text.txt";
        string PathOfTextFilePaths                  = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Text Files Path.txt";
        string QueryForRandomResponsePath           = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Query To Random Response.txt";
        string RandomResponsesFileLocationsPath     = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Random Responses File Locations.txt";
        string ListOfSchedulesPath                  = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Schedule.txt";
        string ListOfRemindersPath                  = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Reminders.txt";
        
        public Main()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Minimized;
            this.FormBorderStyle = FormBorderStyle.SizableToolWindow;
            this.ShowInTaskbar = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            TimeUpdater.Tick += new EventHandler(TimeUpdater_Tick);
            TimeUpdater.Interval = 1800000;
            TimeUpdater.Start();

            ActualMusicFileTitles           = Directory.GetFiles(ActualMusicDirectory, "*.mp3", SearchOption.TopDirectoryOnly);
            ActualMusicFilePaths            = Directory.GetFiles(ActualMusicDirectory, "*.mp3", SearchOption.TopDirectoryOnly);
            
            if (!Directory.Exists(directoryName))               { Directory.CreateDirectory(directoryName); }
            if (!Directory.Exists(phrase1Dir))                  { Directory.CreateDirectory(phrase1Dir); }
            if (!Directory.Exists(phrase2Dir))                  { Directory.CreateDirectory(phrase2Dir); }
            if (!Directory.Exists(randomResponseDir))           { Directory.CreateDirectory(randomResponseDir); }
            if (!File.Exists(ChatQueriesPath))                  { File.Create(ChatQueriesPath); }
            if (!File.Exists(ChatResponsesPath))                { File.Create(ChatResponsesPath); }
            if (!File.Exists(WebQueriesPath))                   { File.Create(WebQueriesPath); }
            if (!File.Exists(WebURLPath))                       { File.Create(WebURLPath); }
            if (!File.Exists(shellQueriesPath))                 { File.Create(shellQueriesPath); }
            if (!File.Exists(shellResponsesPath))               { File.Create(shellResponsesPath); }
            if (!File.Exists(shellFilePath))                    { File.Create(shellFilePath); }
            if (!File.Exists(DefinitionQueriesPath))            { File.Create(DefinitionQueriesPath); }
            if (!File.Exists(WordDefinitionsPath))              { File.Create(WordDefinitionsPath); }
            if (!File.Exists(ProcessKillQueriesPath))           { File.Create(ProcessKillQueriesPath); }
            if (!File.Exists(ProcessNamesPath))                 { File.Create(ProcessNamesPath); }
            if (!File.Exists(ReadQueriesPath))                  { File.Create(ReadQueriesPath); }
            if (!File.Exists(ReadingFilesPath))                 { File.Create(ReadingFilesPath); }
            if (!File.Exists(SendkeyQueriesPath))               { File.Create(SendkeyQueriesPath); }
            if (!File.Exists(SendKeysCodePath))                 { File.Create(SendKeysCodePath); }
            if (!File.Exists(MusicNamesPath))                   { File.Create(MusicNamesPath); }
            if (!File.Exists(MusicFilePaths))                   { File.Create(MusicFilePaths); }
            if (!File.Exists(IdeaQueryPath))                    { File.Create(IdeaQueryPath); }
            if (!File.Exists(FirstPhrasePath))                  { File.Create(FirstPhrasePath); }
            if (!File.Exists(SecondPhrasePath))                 { File.Create(SecondPhrasePath); }
            if (!File.Exists(PathOfQueryToViewText))            { File.Create(PathOfQueryToViewText); }
            if (!File.Exists(PathOfTextFilePaths))              { File.Create(PathOfTextFilePaths); }
            if (!File.Exists(PathOfQueryToViewText))            { File.Create(PathOfQueryToViewText); }
            if (!File.Exists(PathOfTextFilePaths))              { File.Create(PathOfTextFilePaths); }
            if (!File.Exists(QueryForRandomResponsePath))       { File.Create(QueryForRandomResponsePath); }
            if (!File.Exists(RandomResponsesFileLocationsPath)) { File.Create(RandomResponsesFileLocationsPath); }
            if (!File.Exists(ListOfSchedulesPath))              { File.Create(ListOfSchedulesPath); }
            if (!File.Exists(ListOfRemindersPath))              { File.Create(ListOfRemindersPath); }

            try
            {

                ListOfConstantQueries               = File.ReadAllLines(ConstantQueriesPath);
                ListOfChatQueries                   = File.ReadAllLines(ChatQueriesPath);
                ListOfChatResponse                  = File.ReadAllLines(ChatResponsesPath);
                ListOfWebQueries                    = File.ReadAllLines(WebQueriesPath);
                ListOfWebURL                        = File.ReadAllLines(WebURLPath);
                ListOfShellQueries                  = File.ReadAllLines(shellQueriesPath);
                ListOfShellResponses                = File.ReadAllLines(shellResponsesPath);
                ListOfShellPath                     = File.ReadAllLines(shellFilePath);
                ListOfDefinitionQueries             = File.ReadAllLines(DefinitionQueriesPath);
                ListOfWordDefinition                = File.ReadAllLines(WordDefinitionsPath);
                ListOfProcessKillQuery              = File.ReadAllLines(ProcessKillQueriesPath);
                ListOfProcessName                   = File.ReadAllLines(ProcessNamesPath);
                ListOfReadQueries                   = File.ReadAllLines(ReadQueriesPath);
                ListOfReadingPath                   = File.ReadAllLines(ReadingFilesPath);
                ListOfSendkeysQueries               = File.ReadAllLines(SendkeyQueriesPath);
                ListOfSendkeysCode                  = File.ReadAllLines(SendKeysCodePath);
                ListOfMusicNames                    = File.ReadAllLines(MusicNamesPath);
                ListOfMusicPaths                    = File.ReadAllLines(MusicFilePaths);
                ListOfIdeaQuery                     = File.ReadAllLines(IdeaQueryPath);
                ListOf1stPhrase                     = File.ReadAllLines(FirstPhrasePath);
                ListOf2ndPhrase                     = File.ReadAllLines(SecondPhrasePath);
                ListOfQueryToViewText               = File.ReadAllLines(PathOfQueryToViewText);
                ListOfTextFilePaths                 = File.ReadAllLines(PathOfTextFilePaths);
                ListOfQueryForRandomResponse        = File.ReadAllLines(QueryForRandomResponsePath);
                ListOfFileLocationsRandomResponses  = File.ReadAllLines(RandomResponsesFileLocationsPath);
                ListOfSchedules                     = File.ReadAllLines(ListOfSchedulesPath);
                ListOfReminders                     = File.ReadAllLines(ListOfRemindersPath);

            }
            catch { }


            try
            {
                ConstantsGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfConstantQueries)));
                speechMemory.LoadGrammar(ConstantsGrammar);
            }catch {}

            try
            {
                ChatGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfChatQueries)));
                speechMemory.LoadGrammar(ChatGrammar);
            }catch { }
			
            try
            {
                WebGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfWebQueries)));
                speechMemory.LoadGrammar(WebGrammar);
            }catch { }

            try
            {
                ShellGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfShellQueries)));
                speechMemory.LoadGrammar(ShellGrammar);
            }
            catch { }

            try
            {
                DefinitionGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfDefinitionQueries)));
                speechMemory.LoadGrammar(DefinitionGrammar);
            }
            catch { }

            try
            {
                KillProcessGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfProcessKillQuery)));
                speechMemory.LoadGrammar(KillProcessGrammar);
            }
            catch { }

            try
            {
                ReadFileGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfReadQueries)));
                speechMemory.LoadGrammar(ReadFileGrammar);
            }
            catch { }

            try
            {
                SendKeysGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfSendkeysQueries)));
                speechMemory.LoadGrammar(SendKeysGrammar);
            }
            catch { }

            try
            {
                MusicGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfMusicNames)));
                speechMemory.LoadGrammar(MusicGrammar);
            }
            catch { }

            try
            {
                RandomizerGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfIdeaQuery)));
                speechMemory.LoadGrammar(RandomizerGrammar);
            }
            catch { }

            try
            {
                DocumentViewerGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfQueryToViewText)));
                speechMemory.LoadGrammar(DocumentViewerGrammar);
            }
            catch { }

            try
            {
                QueryForRandomResponseGrammar = new Grammar(new GrammarBuilder(new Choices(ListOfQueryForRandomResponse)));
                speechMemory.LoadGrammar(QueryForRandomResponseGrammar);
            }
            catch { }

            speechMemory.SetInputToDefaultAudioDevice();
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(CONSTANTS_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(CHAT_SpeechRecognized);
			speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(WEB_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(SHELL_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(DEFINITION_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(READFILE_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(CALCULATE_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(PROCESSKILL_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(SENDKEYS_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(PLAYMUSIC_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(RANDOMIZER_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(OPENFILE_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(QTRR_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(SCHEDULE_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(SYSTEM_TERMINATION);
            speechMemory.RecognizeAsync(RecognizeMode.Multiple);

            activateListening.SetInputToDefaultAudioDevice();
            activateListening.LoadGrammar(new Grammar(new GrammarBuilder(new Choices("active listening"))));
            activateListening.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(activateListening_SpeechRecognized);

            int randomedNum = random.Next(1,5);

            if (randomedNum == 1) {
                LUCY.SpeakAsync("Hello sir.");
                RepeatSpokenWords = "Hello sir.";
            }
            else if(randomedNum == 2){
                LUCY.SpeakAsync("How are you sir?");
                RepeatSpokenWords = "I said, how are you sir?";
            }else{
                if (to.Hour >= 5 && to.Hour < 12)   { LUCY.SpeakAsync("Goodmorning Sir"); RepeatSpokenWords = "I said, good morning."; }
                if (to.Hour >= 12 && to.Hour < 18)  { LUCY.SpeakAsync("Good afternoon Sir"); RepeatSpokenWords = "I said, good afternoon."; }
                if (to.Hour >= 18 && to.Hour < 24)  { LUCY.SpeakAsync("Good evening Sir"); RepeatSpokenWords = "I said, good evening."; }
                if (to.Hour < 5)                    { LUCY.SpeakAsync("Hello sir, why are you still awake?"); RepeatSpokenWords = "Sir I said why are you still awake?"; }
            }
        }


        private void TimeUpdater_Tick(object sender, EventArgs e)
        {
            int randResponse = random.Next(1,10);

            if (to.Hour >= 6 && to.Hour <= 9)
            {
                if (randResponse <= 3) { LUCY.SpeakAsync("Sir, have you had your breakfast already?"); RepeatSpokenWords = "I said, have you had your breakfast already sir?"; }
                else if (randResponse >= 8) { LUCY.SpeakAsync("Sir, are you done eating your breakfast?"); RepeatSpokenWords = "I said, are you done eating your breakfast sir?"; }
                else { LUCY.SpeakAsync("Sir, have you eaten your breakfast already? Just reminding."); RepeatSpokenWords = "I said, have you eaten your breakfast already sir?"; }
            }
            else if (to.Hour >= 11 && to.Hour <= 13)
            {
                if (randResponse <= 3) { LUCY.SpeakAsync("Sir, have you had your lunch already?"); RepeatSpokenWords = "I said, have you had your lunch already sir?"; }
                else if (randResponse >= 8) { LUCY.SpeakAsync("Sir, are you done eating your lunch?"); RepeatSpokenWords = "I said, are you done eating your lunch sir?"; }
                else { LUCY.SpeakAsync("Sir, have you eaten your lunch? Just reminding you."); RepeatSpokenWords = "I said, have you eaten your lunch sir?"; }
            }
            else if (to.Hour >= 18 && to.Hour <= 20)
            {
                if (randResponse <= 3) { LUCY.SpeakAsync("Sir, have you had your dinner already?"); RepeatSpokenWords = "I said, have you had your dinner already sir?"; }
                else if (randResponse >= 8) { LUCY.SpeakAsync("Sir, are you done eating your dinner?"); RepeatSpokenWords = "I said, are you done eating your dinner sir?"; }
                else { LUCY.SpeakAsync("Sir, have you eaten your dinner already?"); RepeatSpokenWords = "I said, have you eaten your dinner already?"; }
            }
            else if (to.Hour <= 5)
            {
                if (randResponse <= 3) { LUCY.SpeakAsync("Sir, it's getting late, are you still not going to sleep?"); }
                else if (randResponse >= 8) { LUCY.SpeakAsync("Sir, it's getting late. Are you not planning to sleep?"); }
                else { LUCY.SpeakAsync("Sir I am just concerned. Are you still not going to sleep?"); }
                RepeatSpokenWords = "I was just asking if are you still not going to sleep sir.";
            }
        }


        void SCHEDULE_SpeechRecognized(object sender, SpeechRecognizedEventArgs scheduleEvent)
        {

            to = DateTime.Now;
            string current_day = to.ToString("dddd");              // the day today
            string current_time = to.GetDateTimeFormats('t')[1];    // current time
            string current_period = to.ToString("tt");                // AM or PM

            string speech = scheduleEvent.Result.Text;

            string status = String.Empty;
            string YourSchedule = String.Empty;
            string NextSchedule = String.Empty;
            int found = 0;
            int index = 0;

            if (speech == "schedule")
            {

                try
                {

                    foreach (string schedule in ListOfSchedules)
                    {

                        if (schedule.Contains(current_day))
                        {
                            string[] DAY = schedule.Split('|');      // to get the day
                            string[] DAY_PERIOD = DAY[1].Split(';');        // to get the day period (am/pm)
                            string[] MORNING = DAY_PERIOD[0].Split(','); // AM SCHEDULES
                            string[] AFTERNOON = DAY_PERIOD[1].Split(','); // PM SCHEDULES
                            int current_hour = Convert.ToInt32(current_time.Substring(0, 2));
                            int current_minute = Convert.ToInt32(current_time.Substring(3, 2));


                            if (current_period == "AM")
                            {

                                int lastMorningScheduleCheck = Convert.ToInt32(MORNING[MORNING.Length - 1].Substring(0, 2));
                                int lastMorningSchedule = 0;

                                if (lastMorningScheduleCheck == 12)
                                {
                                    lastMorningSchedule = Convert.ToInt32(MORNING[MORNING.Length - 2].Substring(0, 2));
                                }
                                else
                                {
                                    lastMorningSchedule = lastMorningScheduleCheck;
                                }

                                foreach (string morningScheds in MORNING)
                                {
                                    int sched_hour = Convert.ToInt32(MORNING[index].Substring(0, 2));
                                    int sched_minute = Convert.ToInt32(MORNING[index].Substring(3, 2));
                                    int result = current_hour - sched_hour;

                                    if (current_hour == sched_hour)
                                    {
                                        int minutesLeft = sched_minute - current_minute;
                                        int minutesLate = current_minute - sched_minute;
                                        string MM = "minutes";

                                        if (current_minute < sched_minute)
                                        {
                                            found = 1;
                                            if (minutesLeft == 1) { MM = "minute"; }

                                            status = "Not yet late, you have " + minutesLeft + " " + MM + " left. Your schedule is, ";
                                            YourSchedule = status + " " + MORNING[index];
                                            break;
                                        }

                                        else if (current_minute > sched_minute)
                                        {
                                            found = 1;
                                            if (minutesLate == 1) { MM = "minute"; }

                                            try
                                            {
                                                if (current_hour == lastMorningSchedule)
                                                {
                                                    NextSchedule = "And that's your last schedule sir.";
                                                }
                                                else
                                                {
                                                    NextSchedule = "And your next schedule is, " + MORNING[index + 1];
                                                }
                                            }
                                            catch
                                            {
                                                if (current_hour == 12 && lastMorningScheduleCheck == 12)
                                                {
                                                    NextSchedule = "And your next schedule is, " + MORNING[0];
                                                }
                                                else
                                                {
                                                    NextSchedule = "That's your last schedule for this morning sir.";
                                                }
                                            }

                                            status = "You are already " + minutesLate + " " + MM + " late sir. Your current schedule is, ";
                                            YourSchedule = status + " " + MORNING[index] + ". " + NextSchedule;

                                            break;
                                        }

                                        else if (current_minute == sched_minute)
                                        {
                                            found = 1;
                                            status = "It's exactly time sir, your schedule is, ";
                                            YourSchedule = status + " " + MORNING[index];
                                            break;
                                        }
                                    }

                                    else if (result <= -1)
                                    {
                                        if (current_hour > lastMorningSchedule)
                                        {
                                            found = 0;
                                        }
                                        else
                                        {
                                            found = 1;
                                            status = "Your next schedule is ";
                                            YourSchedule = status + " " + MORNING[index];
                                            break;
                                        }
                                    }

                                    index++;
                                }

                            }

                            else if (current_period == "PM")
                            {

                                int lastAfternoonScheduleCheck = Convert.ToInt32(AFTERNOON[AFTERNOON.Length - 1].Substring(0, 2));
                                int lastAfternoonSchedule = 0;

                                if (lastAfternoonScheduleCheck == 12)
                                {
                                    lastAfternoonSchedule = Convert.ToInt32(AFTERNOON[AFTERNOON.Length - 2].Substring(0, 2));
                                }
                                else
                                {
                                    lastAfternoonSchedule = lastAfternoonScheduleCheck;
                                }


                                foreach (string afternoonSched in AFTERNOON)
                                {
                                    int sched_hour = Convert.ToInt32(AFTERNOON[index].Substring(0, 2));
                                    int sched_minute = Convert.ToInt32(AFTERNOON[index].Substring(3, 2));
                                    int result = current_hour - sched_hour;

                                    if (current_hour == sched_hour)
                                    {
                                        int minutesLeft = sched_minute - current_minute;
                                        int minutesLate = current_minute - sched_minute;
                                        string MM = "minutes";

                                        if (current_minute < sched_minute)
                                        {
                                            found = 1;
                                            if (minutesLeft == 1) { MM = "minute"; }

                                            status = "Not yet late, you have " + minutesLeft + " " + MM + " left. Your schedule is, ";
                                            YourSchedule = status + " " + AFTERNOON[index];
                                            break;
                                        }

                                        else if (current_minute > sched_minute)
                                        {
                                            found = 1;
                                            if (minutesLate == 1) { MM = "minute"; }

                                            try
                                            {
                                                if (lastAfternoonScheduleCheck == 12 && current_hour == 12)
                                                {
                                                    NextSchedule = "And your next schedule is , " + AFTERNOON[0];
                                                }
                                                else if (current_hour == lastAfternoonSchedule && lastAfternoonSchedule != 12)
                                                {
                                                    NextSchedule = "And that's your last schedule sir.";
                                                }
                                                else if (current_hour < lastAfternoonSchedule)
                                                {
                                                    NextSchedule = "And your next schedule is , " + AFTERNOON[index + 1];
                                                }
                                            }
                                            catch
                                            {
                                                NextSchedule = "And that's your last schedule sir.";
                                            }

                                            status = "You are already " + minutesLate + " " + MM + " late sir. Your current schedule is, ";
                                            YourSchedule = status + " " + AFTERNOON[index] + ". " + NextSchedule;

                                            break;
                                        }

                                        else if (current_minute == sched_minute)
                                        {
                                            found = 1;
                                            status = "It's exactly time sir, your schedule is, ";
                                            YourSchedule = status + " " + AFTERNOON[index];
                                            break;
                                        }
                                    }

                                    else if (result <= -1)
                                    {
                                        if (current_hour > lastAfternoonSchedule) { found = 0; }
                                        else
                                        {
                                            found = 1;
                                            status = "Your next schedule is ";
                                            YourSchedule = status + " " + AFTERNOON[index];
                                            break;
                                        }
                                    }

                                    index++;
                                }

                                if (current_hour == 12 && lastAfternoonScheduleCheck != 12 && found == 0)
                                {
                                    found = 1;
                                    status = "Your next schedule is ";
                                    YourSchedule = status + " " + AFTERNOON[0];
                                }
                            }
                        }
                    }

                    // ################################################################# //

                    if (found == 1)
                    {
                        LUCY.SpeakAsync(YourSchedule);
                        RepeatSpokenWords = YourSchedule;
                    }
                    else
                    {
                        LUCY.SpeakAsync("No more schedule sir.");
                        RepeatSpokenWords = "I said, no more schedule sir.";
                    }

                    // ################################################################# //

                }
                catch
                {
                    LUCY.SpeakAsync("Schedule is currently empty.");
                    RepeatSpokenWords = "Schedule is currently empty.";
                }
            }
        }


        void MUTE_SOUND()
        {
            SendMessageW(this.Handle, WM_APPCOMMAND, this.Handle, (IntPtr)APPCOMMAND_VOLUME_MUTE);
        }

        void UNMUTE_SOUND()
        {
            SendMessageW(this.Handle, WM_APPCOMMAND, this.Handle, (IntPtr)APPCOMMAND_VOLUME_UP);
        }

    }
}