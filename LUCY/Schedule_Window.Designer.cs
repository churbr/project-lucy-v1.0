﻿namespace LUCY
{
    partial class Schedule_Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Schedule_Window));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DayPicker = new System.Windows.Forms.DateTimePicker();
            this.MorningTextBox = new System.Windows.Forms.RichTextBox();
            this.AfternoonTextBox = new System.Windows.Forms.RichTextBox();
            this.Save_Schedule = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "DAY:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(161, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "MORNING SCHEDULE:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(181, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "AFTERNOON SCHEDULE:";
            // 
            // DayPicker
            // 
            this.DayPicker.CustomFormat = "dddd";
            this.DayPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DayPicker.Location = new System.Drawing.Point(71, 21);
            this.DayPicker.Name = "DayPicker";
            this.DayPicker.Size = new System.Drawing.Size(120, 26);
            this.DayPicker.TabIndex = 3;
            // 
            // MorningTextBox
            // 
            this.MorningTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.MorningTextBox.Location = new System.Drawing.Point(71, 94);
            this.MorningTextBox.Name = "MorningTextBox";
            this.MorningTextBox.Size = new System.Drawing.Size(340, 110);
            this.MorningTextBox.TabIndex = 5;
            this.MorningTextBox.Text = "";
            // 
            // AfternoonTextBox
            // 
            this.AfternoonTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.AfternoonTextBox.Location = new System.Drawing.Point(71, 253);
            this.AfternoonTextBox.Name = "AfternoonTextBox";
            this.AfternoonTextBox.Size = new System.Drawing.Size(340, 110);
            this.AfternoonTextBox.TabIndex = 6;
            this.AfternoonTextBox.Text = "";
            // 
            // Save_Schedule
            // 
            this.Save_Schedule.Location = new System.Drawing.Point(151, 369);
            this.Save_Schedule.Name = "Save_Schedule";
            this.Save_Schedule.Size = new System.Drawing.Size(167, 48);
            this.Save_Schedule.TabIndex = 7;
            this.Save_Schedule.Text = "SAVE SCHEDULE";
            this.Save_Schedule.UseVisualStyleBackColor = true;
            this.Save_Schedule.Click += new System.EventHandler(this.Save_Schedule_Click);
            // 
            // Schedule_Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(451, 429);
            this.Controls.Add(this.Save_Schedule);
            this.Controls.Add(this.AfternoonTextBox);
            this.Controls.Add(this.MorningTextBox);
            this.Controls.Add(this.DayPicker);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Schedule_Window";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ADD SCHEDULE";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker DayPicker;
        private System.Windows.Forms.RichTextBox MorningTextBox;
        private System.Windows.Forms.RichTextBox AfternoonTextBox;
        private System.Windows.Forms.Button Save_Schedule;
    }
}