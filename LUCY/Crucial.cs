﻿using System.Collections.Generic;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Data;
using System.IO;
using System;

namespace LUCY
{
    public partial class Main : Form
    {
        void QTRR_SpeechRecognized(object sender, SpeechRecognizedEventArgs QueryToRandomEvent)
        {
            string speech = QueryToRandomEvent.Result.Text;
            index = 0;

            try {
                foreach (string query in ListOfQueryForRandomResponse)
                {
                    if (query == speech)
                    {
                        randomResponses = File.ReadAllLines(ListOfFileLocationsRandomResponses[index]);
                        int randResponseLine = random.Next(0, randomResponses.Length);
                        RepeatSpokenWords = "I said " + randomResponses[randResponseLine];

                        LUCY.Speak(randomResponses[randResponseLine]);
                    }
                    index++;
                }
            }
            catch { }
        }



        void CHAT_SpeechRecognized(object sender, SpeechRecognizedEventArgs chatEvent)
        {
            string speech = chatEvent.Result.Text;
            index = 0;
            try
            {
                foreach (string line in ListOfChatQueries)
                {
                    if (line == speech)
                    {
                        RepeatSpokenWords = "I said " + ListOfChatResponse[index];
                        LUCY.SpeakAsync(ListOfChatResponse[index]);
                    }
                    index++;
                }
            }
            catch { }
        }



        void WEB_SpeechRecognized(object sender, SpeechRecognizedEventArgs webEvent)
        {
            string speech = webEvent.Result.Text;
            index = 0;
            
            if(speech == "open browsing"){
                RepeatSpokenWords = "I said what website sir.";
                LUCY.SpeakAsync("What website am I going to access sir?");
                EventStatus = "Website Access";
            }

            if (EventStatus == "Website Access")
            {
                try
                {
                    foreach (string line in ListOfWebQueries)
                    {
                        if (line == speech)
                        {
                            string link = ListOfWebURL[index];
                            string[] trimmed = link.Split('.');

                            Process.Start(ListOfWebURL[index]);
                            RepeatSpokenWords = trimmed[1] + "." + trimmed[2] + " sir.";
                            LUCY.SpeakAsync("Accessing site " + trimmed[1] + "." + trimmed[2]);
                            EventStatus = String.Empty;
                        }
                        index++;
                    }
                }
                catch { }
            }

        }



        void SHELL_SpeechRecognized(object sender, SpeechRecognizedEventArgs shellEvent)
        {
            string speech = shellEvent.Result.Text;
            index = 0;
            try
            {
                foreach (string line in ListOfShellQueries)
                {
                    if (line == speech)
                    {
                        Process.Start(ListOfShellPath[index]);
                        RepeatSpokenWords = "I said " + ListOfShellResponses[index];
                        LUCY.SpeakAsync(ListOfShellResponses[index]);
                    }
                    index++;
                }
            }
            catch { }
        }



        void DEFINITION_SpeechRecognized(object sender, SpeechRecognizedEventArgs definitionEvent)
        {
            string speech = definitionEvent.Result.Text;
            index = 0;

            try
            {
                foreach (string line in ListOfDefinitionQueries)
                {
                    if (line == speech)
                    {
                        RepeatSpokenWords = ListOfWordDefinition[index];
                        LUCY.SpeakAsync(ListOfWordDefinition[index]);
                    }
                    index++;
                }
            }
            catch { }
        }
        


        void SENDKEYS_SpeechRecognized(object sender, SpeechRecognizedEventArgs sendKeysEvent)
        {
            string speech = sendKeysEvent.Result.Text;
            index = 0;

            try
            {
                foreach(string line in ListOfSendkeysQueries)
                {
                    if(line == speech)
                    {
                        SendKeys.Send(ListOfSendkeysCode[index]);
                    }
                    index++;
                }
            }
            catch {}
        }



        void READFILE_SpeechRecognized(object sender, SpeechRecognizedEventArgs readFileEvent)
        {
            string speech = readFileEvent.Result.Text;
            index = 0;
            try
            {
                foreach (string line in ListOfReadQueries)
                {
                    if (line == speech)
                    {
                        toBeRead = File.ReadAllText(ListOfReadingPath[index]);
                        RepeatSpokenWords = toBeRead;
                        LUCY.SpeakAsync(toBeRead);
                    }
                    index++;
                }
            }
            catch { }
        }



        void PROCESSKILL_SpeechRecognized(object sender, SpeechRecognizedEventArgs killProcessEvent)
        {
            string speech = killProcessEvent.Result.Text;
            index = 0;
            try
            {
                foreach (string line in ListOfProcessKillQuery)
                {
                    if (line == speech)
                    {
                        ProcessWindow = ListOfProcessName[index];
                        RepeatSpokenWords = "Process " + ListOfProcessName[index] + " ended sir.";
                        LUCY.SpeakAsync("Process " + ListOfProcessName[index] + "ended.");
                        CloseWindow();
                    }
                    index++;
                }
            }
            catch { }
        }



        void SYSTEM_TERMINATION(object sender, SpeechRecognizedEventArgs systemTerminateEvent)
        {
            string[] toTerminate = {"shutdown", "logoff", "restart"};

            string speech = systemTerminateEvent.Result.Text;

            if(toTerminate.Any(speech.Contains)){
                EventStatus = "System Termination";
                TerminationEvent = speech;
                RepeatSpokenWords = "I was asking if are you sure to " + speech + " sir?";
                LUCY.Speak("Are you sure to " + speech + " sir?");
            }

            if (speech == "yes" && EventStatus == "System Termination")
            {

                if(TerminationEvent == "shutdown")
                {
                    RepeatSpokenWords = "Shutting down in T. Minus. Ten seconds.";
                    LUCY.SpeakAsync("Shutting down in T. Minus. Ten seconds.");
                    Process.Start("shutdown", "/f /s /t 30");
                }
                
                else if(TerminationEvent == "logoff")
                {
                    RepeatSpokenWords = "Logging off.";
                    LUCY.SpeakAsync("Logging off.");
                    Process.Start("shutdown", "/l");
                }

                else if (TerminationEvent == "restart")
                {
                    RepeatSpokenWords = "Rebooting in T. Minus. Ten seconds.";
                    LUCY.SpeakAsync("Rebooting in T. Minus. Ten seconds.");
                    Process.Start("shutdown", "/r /t 30");
                }

                TerminationEvent = String.Empty;
                EventStatus = String.Empty;
            }

            else if (speech == "no" && EventStatus == "System Termination")
            {
                RepeatSpokenWords = "I've just cancelled the system termination sir.";
                LUCY.SpeakAsync("Alright then.");
                TerminationEvent = String.Empty;
                EventStatus = String.Empty;
            }
        }



        private void CloseWindow()
        {
            Process[] processes = Process.GetProcessesByName(ProcessWindow);

            foreach (Process process in processes)
            {
                process.CloseMainWindow();
            }
        }

        private void Reminders() {

            to = DateTime.Now;
            string dateToday    = to.ToString("MMMM dd yyy");
            string Reminder     = String.Empty;
            int ReminderFound   = 0;

            try
            {

                foreach (string reminder in ListOfReminders)
                {
                    if (reminder.Contains(dateToday) == true)
                    {
                        ReminderFound = 1;
                        string[] getReminder = reminder.Split(':');
                        Reminder = getReminder[1];
                    }
                }

                // ########################################################################### //

                if (ReminderFound == 1)
                {
                    RepeatSpokenWords = "I said, " + Reminder;
                    LUCY.SpeakAsync(Reminder);
                }
                else
                {
                    RepeatSpokenWords = "I said, no reminders for today sir.";
                    LUCY.SpeakAsync("No reminders for today sir.");
                }

                // ########################################################################### //

            }
            catch {
                RepeatSpokenWords = "I said, no reminders for today sir.";
                LUCY.SpeakAsync("No reminders for today sir.");
            }

        }
    }
}