﻿using System.Collections.Generic;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System;

namespace LUCY
{
    public partial class Main : Form
    {
        void PLAYMUSIC_SpeechRecognized(object sender, SpeechRecognizedEventArgs playMusicEvent)
        {
            
            string[] speechSongNames        = {
                                                  "give me a list",
                                                  "what are the titles",
                                                  "title of the songs",
                                                  "list all the songs",
                                                  "tell me the names of the songs"
                                              };
           
            string[] speechToRandomPlay     = {
                                                  "any songs",
                                                  "any kind",
                                                  "your choice",
                                                  "anything you like",
                                                  "you choose","depends on you",
                                                  "play randomly","random play",
                                                  "you decide"
                                              };

            string[] speechToPlayAll        = {
                                                  "play all",
                                                  "play all songs",
                                                  "play all music",
                                                  "play all of it"
                                              };

            string[] speechNextRandom       = {
                                                  "next music",
                                                  "next song",
                                                  "play next",
                                                  "change music"
                                              };

            string[] speechToPlayMusic      = {
                                                  "play music",
                                                  "play a song"
                                              };

            string speech                   = playMusicEvent.Result.Text;
            int line                        = random.Next(1,5);
            index = 0;

            try
            {
                if (speechToPlayMusic.Any(speech.Contains))
                {
                    RepeatSpokenWords = "I was asking what song would you like me to play sir.";

                    if (line == 1)
                    {
                        LUCY.SpeakAsync("Which song sir?");
                    }
                    else if (line == 2)
                    {
                        LUCY.SpeakAsync("What song would you like me to play sir?");
                    }
                    else
                    {
                        LUCY.SpeakAsync("What's the name of the song?");
                    }

                    musicEvent = "play music";
                }


                if (speechNextRandom.Any(speech.Contains))
                {
                    musicEvent = "next random";
                }

                try
                {
                    if (ListOfMusicNames.Any(speech.Contains))
                    {
                        LUCY.SpeakAsyncCancelAll();
                        musicEvent = "play immediately";
                    }
                }
                catch { }

                // #############################################################################

                if (musicEvent == "play immediately")
                {
                    foreach (string musicName in ListOfMusicNames)
                    {
                        if (musicName == speech)
                        {
                            Process.Start(ListOfMusicPaths[index]);
                            RepeatSpokenWords = "I just played the song " + ListOfMusicNames[index];
                            LUCY.SpeakAsync("Playing " + ListOfMusicNames[index]);
                            musicEvent = String.Empty;
                        }
                        index++;
                    }
                    musicEvent = String.Empty;
                }



                if (musicEvent == "next random")
                {
                    if (ListOfMusicNames.Length > 0)
                    {
                        line = random.Next(0, ListOfMusicNames.Length);
                        Process.Start(ListOfMusicPaths[line]);
                        RepeatSpokenWords = "I just played the song " + ListOfMusicNames[line];
                        LUCY.SpeakAsync("Next music will be " + ListOfMusicNames[line]);
                        musicEvent = String.Empty;
                    }
                }

                // #############################################################################

                if (musicEvent == "play music")
                {
                    if (speechToRandomPlay.Any(speech.Contains))
                    {
                        musicEvent = "random play";
                    }

                    else if (speechSongNames.Any(speech.Contains))
                    {
                        musicEvent = "song names";
                    }

                    else if (speechToPlayAll.Any(speech.Contains))
                    {
                        musicEvent = "play all";
                    }

                    else
                    {
                        foreach (string musicName in ListOfMusicNames)
                        {
                            if (musicName == speech)
                            {
                                Process.Start(ListOfMusicPaths[index]);
                                RepeatSpokenWords = "I just played the song " + ListOfMusicNames[index];
                                LUCY.SpeakAsync("Playing " + ListOfMusicNames[index]);
                                musicEvent = String.Empty;
                            }
                            index++;
                        }
                    }
                }


                if (musicEvent == "random play")
                {
                    if (ListOfMusicNames.Length > 0)
                    {
                        line = random.Next(0, ListOfMusicNames.Length);
                        Process.Start(ListOfMusicPaths[line]);
                        RepeatSpokenWords = "I just played the song " + ListOfMusicNames[line];
                        LUCY.SpeakAsync("Okay, I will play " + ListOfMusicNames[line]);
                    }
                    musicEvent = String.Empty;
                }


                if (musicEvent == "song names")
                {
                    if (ListOfMusicNames.Length > 0)
                    {
                        RepeatSpokenWords = String.Empty;
                        LUCY.SpeakAsync("These are the songs available, ");
                        foreach (string musicName in ListOfMusicNames)
                        {
                            RepeatSpokenWords += musicName + ", ";
                            LUCY.SpeakAsync(musicName);
                        }
                    }
                    else
                    {
                        RepeatSpokenWords = "I said, there are no songs in your music folder sir.";
                        LUCY.SpeakAsync("My apologies sir, but there are currently no songs in your music folder.");
                    }
                    musicEvent = String.Empty;
                }

                if (musicEvent == "play all")
                {

                    if (ListOfMusicNames.Length == 0)
                    {
                        RepeatSpokenWords = "I said I can't play anything yet.";
                        LUCY.SpeakAsync("Sir I cannot play any songs yet. Music folder is empty.");
                    }
                    else
                    {
                        RepeatSpokenWords = "I just played all of your music sir.";
                        LUCY.SpeakAsync("Playing all music.");
                        foreach (string music in ListOfMusicPaths)
                        {
                            Process.Start(music);
                        }
                    }
                    musicEvent = String.Empty;
                }
            }
            catch { }
        }
    }
}