﻿using System.Collections.Generic;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.IO;
using System;

namespace LUCY
{
    public partial class Reminder_Window : Form
    {
        string SaveLocation = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Reminders.txt";

        public Reminder_Window() { InitializeComponent(); }

        private void Save_Reminder_Click(object sender, EventArgs e)
        {
            using (StreamWriter writer = File.AppendText(SaveLocation))
            {
                writer.WriteLine(datePicked.Value.ToString("MMMM dd yyyy") + ":" + ReminderInput.Text);
                ReminderInput.Text = String.Empty;
            }
            MessageBox.Show("Reminder Added.");
        }
    }
}